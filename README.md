## Motivation

RelevancerAPI aims at identifying relevant content in social media streams. RelevancerAPI provides an abstraction layer for the Relevancer website.

## Contributors

Ali Hürriyetoglu ([@hurrial](https://twitter.com/hurrial)), Mustafa Erkan Başar ([@me_basar](https://twitter.com/me_basar)), Nelleke Oostdijk, Antal van den Bosch ([@antalvdb](https://twitter.com/antalvdb)), Aslıhan Arslan ([@miniminiibirkus](https://twitter.com/miniminiibirkus)), Uğur Özcan ([@uozcan12](https://twitter.com/uozcan12)).

## Publications

Submitted.

## Demo

http://relevancer.science.ru.nl/

## License

Licensed under GPLv3 (See http://www.gnu.org/licenses/gpl-3.0.html)


## Acknowledgements
This project was supported by Floodtags ([@FloodTags](https://twitter.com/FloodTags)) and the Dutch national programme COMMIT ([@COMMIT_nl](https://twitter.com/COMMIT_nl) as part of the Infiniti project, Work Package ADNEXT ([@Adnext_Commit](https://twitter.com/Adnext_Commit)).
