from flask import Flask
app = Flask(__name__)
from flask import request
from flask import jsonify
from flask import render_template

import sys
import getopt
import random
import pymongo
from bson.son import SON
from bson.objectid import ObjectId
import ast
import json
import re
import datetime
from collections import Counter
from bson import json_util
from itertools import chain
from dateutil import parser

client = pymongo.MongoClient('localhost', 27018) #for applejack you can use 27018
#db=client['relevancer']
#app.config['DOMAIN']["mini"]["pagination"] = False   #to cancel pagination


@app.route('/')
def hello_world():
    return render_template('index.html')

@app.route("/dbinfo")
def allDBs():
    '''
    This method shows information of database with their collections
    '''
    d = dict((db, [collection for collection in client[db].collection_names()])
             for db in client.database_names())
    return jsonify(d)

@app.route("/dbinfo/<mydb>")
def collectionsDB(mydb):
    """
    Displays all database information with collections.
    """
    if client[mydb].collection_names():
        collectionlist=[c for c in client[mydb].collection_names() if c != "system.indexes"]
        return jsonify(collections=collectionlist)
    else:
        return jsonify(Error="There is not any database in this name. Requested name:"+mydb)


@app.route("/getdata/<mydb>/<collname>")
def collnames(mydb, collname):
    '''
    This method returns all of the documents which is in the given collection
    '''
    documents = []
    for document in client[mydb][collname].find({}, {"_id":0}):
        documents.append(document)

    if documents:

        return jsonify(documents=documents)
    else:
        return jsonify(Error="There is not any collection in this name. Requested database and collection:"+mydb+", "+collname)



@app.route("/count/<mydb>/<collname>")
def collname(mydb, collname):
    '''
    This method returns number of the documents which is in the given collection
    '''
    countdb=client[mydb][collname].count();
    if countdb>0:
        return jsonify(collection_size=countdb)
    else:
        return jsonify(Error="There is not any collection in this name. Requested database and collection:"+mydb+", "+collname)


@app.route("/count/<mydb>/<collname>/<opt>")
def countlabeled(mydb, collname, opt):
    '''
    This method returns number of tweets that label field exists or not.
    '''
    if opt == 'labeled':
        countdb=client[mydb][collname].find({"label":{"$exists":True}}).count()

    elif opt == 'unlabeled':
        countdb=client[mydb][collname].find({"label":{"$exists":False}}).count()

    return jsonify(collection_size=countdb)


@app.route("/count/<mydb>/<collname>/perinterval/since=<sincestr>&until=<untilstr>")
def count_per_interval(mydb, collname, sincestr, untilstr):
    '''
    This method returns number of tweets per time intervals.
    '''
    since = parser.parse(sincestr)
    until = parser.parse(untilstr)

    count = client[mydb][collname].find({"date":{"$gte": since, "$lt": until}}, {'_id':0}).count()

    return jsonify(collection_size=count)


@app.route("/getdata/<mydb>/<collname>/perinterval/since=<sincestr>&until=<untilstr>&hasLocation=<hasLocation>")
def data_per_interval(mydb, collname, sincestr, untilstr, hasLocation):
    '''
    This method returns number of tweets per time intervals.
    '''
    since = parser.parse(sincestr)
    until = parser.parse(untilstr)

    if(hasLocation=='True'):
        documents = list(client[mydb][collname].find({'$and':[{"date":{"$gte": since, "$lt": until}},{'locations':{'$not':{'$size':0}}}]}, {'_id':0}))
    else:
        documents = list(client[mydb][collname].find({"date":{"$gte": since, "$lt": until}}, {'_id':0}))

    return jsonify(documents=documents)

@app.route("/getdatelist/<mydb>/<collname>/perinterval/since=<sincestr>&until=<untilstr>&hasLocation=<hasLocation>&skip=<skip>")
def intervals_by_date(mydb, collname, sincestr, untilstr, hasLocation, skip):
    '''
    This method returns the list of dates by which the dataset can be binned into time intervals of 1000 tweets each.
    '''
    since = parser.parse(sincestr)
    until = parser.parse(untilstr)
    current_since = since
    skip = int(skip)

    ds = []
    ds.append(since.isoformat())
    if(hasLocation=='True'):
        documents = client[mydb][collname].find({'$and':[{"date":{"$gte": current_since, "$lt": until}},{'locations':{'$not':{'$size':0}}}]}, {'date':1})#.sort('date',1).limit(100)
    else:
        documents = client[mydb][collname].find({"date":{"$gte": current_since, "$lt": until}}, {'date':1})
    docs_sorted = sorted([document['date'] for document in documents])
    numdocs = len(docs_sorted)
    docindex = skip
    while docindex < numdocs:
        ds.append(docs_sorted[docindex].isoformat())
        docindex += skip
    ds.append(until.isoformat())

    return jsonify(dates=ds)

@app.route("/getoverviewdata/<mydb>/<collname>/perinterval/since=<sincestr>&until=<untilstr>&hasLocation=<hasLocation>&entries=<entries>")
def get_overview_data(mydb, collname, sincestr, untilstr, hasLocation, entries):
    '''
    This method divides tweets in a given date ranges into intervals with an equal duration, and for each interval boundary returns the date and time as well as the tweet counts.
    '''
    since = parser.parse(sincestr)
    until = parser.parse(untilstr)
    timerange = until-since
    days, seconds = timerange.days, timerange.seconds
    hours = days * 24 + seconds // 3600
    entries = int(entries)

    interval = datetime.timedelta(hours = int(hours / entries))
    cursor = since
    overview = []
    while cursor < until - interval:
        cursor_new = cursor + interval
        count = client[mydb][collname].find({"date":{"$gte": cursor, "$lt": cursor_new}}, {'_id':0}).count()
        overview.append({'date':cursor_new.strftime('%Y-%m-%d, %H:%M:%S'),'value': count})
        cursor = cursor_new
    if not cursor_new == until:
        final_count = client[mydb][collname].find({"date":{"$gte": cursor_new, "$lt": until}}, {'_id':0}).count()
        overview.append({'date':until.strftime('%Y-%m-%d, %H:%M:%S'),'value': final_count})

    return jsonify(overviewdata=overview)

@app.route("/addcollection/<mydb>/<collname>")
def addColl(mydb, collname):
    '''
    This method adds a collection in to the database.
    '''
    if collname not in client[mydb].collection_names():
        addCol=client[mydb][collname].insert({}, {"_id":0});
        return jsonify(collectionAdded=collname)
    else:
        return jsonify(Error=mydb+" already contain "+collname)


@app.route("/dropcollection/<mydb>/<collname>")
def dropColl(mydb, collname):
    '''
    This method drops a collection from database
    '''
    if collname in client[mydb].collection_names():
        dropCol=client[mydb][collname].drop()
        return jsonify(collectionDropped=collname)
    else:
        return jsonify(Error=mydb+" does not contain "+collname)



@app.route("/remove/<mydb>/<collname>/collection")
def delColl(mydb, collname):
    '''
    This method removes collection form the database
    '''
    if collname in client[mydb].collection_names():
        delCol=client[mydb][collname].remove()
        return jsonify(collectionDeleted=collname)
    else:
        return jsonify(Error=mydb+" does not contain "+collname)



@app.route("/insert/<mydb>/<collname>", methods=['POST'])
def insert(mydb, collname):
    '''
    This method inserts a new collection to database
    '''
    insert_data = json.loads(request.json)["documents"]

    # Convert iso formated string dates to datetime objects.
    # Can not send dates as datetime at the beginning because of json.dumps
    for d in insert_data:
        if('date' in d):
            d.update((k, parser.parse(v)) for k, v in d.items() if k == 'date')

    inserted=client[mydb][collname].insert(insert_data) # If doesn't work properly, try insertMany

    return jsonify(objid=str(inserted))



@app.route("/update/<mydb>/<collname>", methods=['POST'])
def update(mydb, collname):
    '''
    This method modifies an existing document based on the objectID in a collection.
    '''
    update_data = json.loads(request.json)
    for k, v in update_data.items():
        if k!="objid":
            if isinstance(v, list):
                updated=client[mydb][collname].update_one({"_id":ObjectId(update_data["objid"])}, {'$addToSet':{k:{'$each':v}}})
            else:
                updated=client[mydb][collname].update_one({"_id":ObjectId(update_data["objid"])}, {'$addToSet':{k:v}})
    return jsonify(objid=update_data["objid"])


@app.route("/update/<mydb>/<collname>/label/<label>/<objectid>")
def updatelabel(mydb, collname, label, objectid):
    '''
    Updates the label in the given document.
    '''
    updated=client[mydb][collname].update({"_id":ObjectId(objectid)},{'$set':{"label":label}});
    return jsonify(label=label)


@app.route("/remove/<mydb>/<collname>/label/<objectid>")
def removed(mydb, collname, objectid):
    '''
    Removes the label from the given document.
    '''
    removed=client[mydb][collname].update({"_id":ObjectId(objectid)},{'$unset':{"label":1}});
    return jsonify(removed=removed)


@app.route("/remove/<mydb>/<collname>/field/<field>")
def field(mydb, collname, field):
    '''
    Removes a field from every document in a collection.
    '''
    field=client[mydb][collname].update({}, {'$unset':{field:1}}, multi=True);
    return jsonify(field=field)


@app.route("/remove/<mydb>/<collname>/<username>")
def remove(mydb, collname, username):
    '''
    This method removes a username from collection.
    '''
    removed=client[mydb][collname].remove({"username":username});
    return jsonify(removed=username)


@app.route("/getsummary/<mydb>/<collname>/field/<field>")
def keywords(mydb,collname, field) :
    '''
    This method finds the unique (distinct) values for a specified field across a single collection
    '''
    key=client[mydb][collname].distinct(field);
    return jsonify(field=key)



@app.route("/getsummary/<mydb>/<collname>/<field>/num_tweets")
def aggregation(mydb, collname, field):
    '''
    This method finds the unique values for a specified field with number of tweets across a single collection
    '''
    new_list=[]
    for document in client[mydb][collname].aggregate([{'$match' :{field :{'$exists':True}}},{"$group" : {"_id" : "$"+field, "num_tweets" : {"$sum" : 1}}}]):
        document[field]=document.pop('_id')
        new_list.append(document)
    if 'floodIndonesiaFeb2017_TNC-Flood' in collname:
        labels = ['flood', 'flood_related', 'mixed', 'irrelevant']
        existing_labels = [label_dict['label'] for label_dict in new_list]
        for label in labels:
            if label not in existing_labels:
                new_list.append({'label': label, 'num_tweets': 0})
    if new_list:
        return jsonify(tweets=new_list)
    else:
        return jsonify(tweets=None)


@app.route("/getdata/<mydb>/<collname>/field/<field>")
def getsinglefieldvalue(mydb, collname, field):
    '''
    This method returns specified field across a single collection
    '''
    field_values=[]
    field_found=0
    field_not_found=0
    for d in client[mydb][collname].find({}, {"_id":0, field:1}):
        if field in d:
            field_values.append(d[field])
            field_found+=1
        else:
            field_not_found+=1
            # Do if the field is not found
    if field_found==0:
        return jsonify(Error="There is not any document that contains this field.Count of other documents:"+str(field_not_found))
    return jsonify(value_list=field_values)



@app.route("/getdata/<mydb>/<collname>/objectid/<objectid>")
def getdatabyid(mydb, collname, objectid):
    '''
    This method returns a single document according to ObjectId.
    '''
    objectdata = list(client['relevancer']['QueryList'].find({"_id":ObjectId(objectid)},{"_id":0}))
    if objectdata:
        return json.dumps(objectdata[0], default=json_util.default)
    else:
        return jsonify(Error="There is not any collection name")


@app.route("/getdata/<mydb>/<collname>/filtername/<filtername>")
def getdatabyfiltername(mydb, collname, filtername):
    '''
    This method returns the filter related with the given filter name
    '''
    objectdata = list(client['relevancer']['QueryList'].find({"filtername":filtername}))
    return json.dumps(objectdata[0], default=json_util.default)


@app.route("/getdata/<mydb>/<collname>/usernames/<username>")
def agg(mydb, collname, username):
    '''
    This method returns tweets of the given user.
    '''
    documents = []
    for document in client[mydb][collname].find({'source.user.name': username}, {"_id":0}):
        documents.append(document)
    if documents:
        return jsonify(tweets=documents)
    else:
        return jsonify(Error="There is not such a user.")

@app.route("/getdata/<mydb>/<collname>/keywords/<keyword>")
def unames(mydb, collname, keyword):
    '''
    This method returns tweets of the given keyword.
    '''
    documents=[]
    for document in client[mydb][collname].find({'keywords': keyword}, {"_id":0}):
        documents.append(document)
    if documents:
        return jsonify(tweets=documents)
    else:
        return jsonify(Error="There is not such a keyword.")

# Not in use
@app.route("/<mydb>/<collname>/usernames")
def uname(mydb, collname) :
    '''
    This method returns list of usernames
    '''
    use=client[mydb][collname].distinct("source.user.name");
    return jsonify(usernames=use)

@app.route("/mostfrequent/<mydb>/<collname>/usernames", methods=['POST'])
def mostlyfrequentusers(mydb, collname):
    '''
    This method returns mostfrequent usernames from a collection.
    '''
    incoming = json.loads(request.json)
    keyword = incoming['keyword']
    opt = incoming['opt']
    n=-1
    if "n" in incoming and incoming["n"].isdigit():
        n=int(incoming['n'])
        if opt=="urls":
            e=client[mydb][collname].aggregate([{'$match' :{'$and':[{'urls':{'$exists':True, '$not':{'$size':0}}},{'keywords':{'$in':[keyword]}}]}},{"$group" : {"_id" : "$source.user.name", "numtweet" : {"$sum" : 1}}}, {"$sort": SON([("numtweet", -1), ("_id", -1)])}])
    #Aggregation method returns the following error:"aggregation result exceeds maximum document size (16MB)."

        elif opt=="photos":
            e=client[mydb][collname].aggregate([{'$match' :{'$and':[{'photos':{'$exists':True, '$not':{'$size':0}}},{'keywords':{'$in':[keyword]}}]}},{"$group" : {"_id" : "$source.user.name", "numtweet" : {"$sum" : 1}}}, {"$sort": SON([("numtweet", -1), ("_id", -1)])}])
            
        elif opt=="nophotosurls":
            e=client[mydb][collname].aggregate([{'$match': {'$and':[{'photos':{'$size':0}},{'urls':{'$size':0}},{'keywords':{'$in':[keyword]}}]}},{"$group" : {"_id" : "$source.user.name", "numtweet" : {"$sum" : 1}}}, {"$sort": SON([("numtweet", -1), ("_id", -1)])}])
        # To get the most freq users all over the collection, an else could be written here
    elif "n" in incoming:
        return jsonify(Error="n must be a digit.")

    listt=[]
    i=0
    for document in list(e):
        document['username']=document.pop('_id')
        exampletweets=list(client[mydb][collname].find({'source.user.name': document["username"]},{'text':1, '_id':0}).limit(10))
        tweets=[etdict['text'] for etdict in exampletweets]
        document['exampletweets'] = tweets
        listt.append(document)
        i+=1
        if i==n:
            #print(listt)
            return jsonify(userlist=listt)
    return jsonify(userlist=listt)

@app.route("/mostfrequent/<mydb>/<collname>/hashtags", methods=['POST'])
def mostlyfrequenthashtags(mydb, collname):
    '''
    This method excludes the tweets of twitter users based on the user list provided by training interface.
    Then returs the most frequent hashtags from the new tweet list(tweetids list).
    '''
    incoming = json.loads(request.json)
    opt = incoming['opt']
    queryid = incoming['objid']
    n=-1
    if "n" in incoming and incoming["n"].isdigit():
        n=int(incoming['n'])
        if opt=="urls":
            lists_url = list(client['relevancer']['QueryList'].find({"_id":ObjectId(queryid)},{'tweets_urls':1,'userlist_urls':1}))
            # print(lists_url[0])
            for doc in lists_url:
                userlist_urls = doc['userlist_urls']
                tweets_urls = doc['tweets_urls']

            e=client[mydb][collname].find({'$and':[{'_id':{'$in':tweets_urls}},{'source.user.name':{'$nin':userlist_urls}}]},{'_id':1, 'text':1})

            tweetids=[]
            tweets=[]
            for doc in e:
                tweets.append(doc['text'])
                tweetids.append(doc['_id'])


            f=client['relevancer']['QueryList'].update_one({"_id":ObjectId(queryid)},{'$addToSet':{'tweets_urls':{'$each':tweetids}}})

        elif opt=="photos":
            lists_photos = list(client['relevancer']['QueryList'].find({"_id":ObjectId(queryid)},{'tweets_photos':1,'userlist_photos':1}))
            for doc in lists_photos:
                userlist_photos = doc['userlist_photos']
                tweets_photos = doc['tweets_photos']

            e=client[mydb][collname].find({'$and':[{'_id':{'$in':tweets_photos}},{'source.user.name':{'$nin':userlist_photos}}]},{'_id':1, 'text':1})

            tweetids=[]
            tweets=[]
            for doc in e:
                tweets.append(doc['text'])
                tweetids.append(doc['_id'])


            f=client['relevancer']['QueryList'].update_one({"_id":ObjectId(queryid)},{'$addToSet':{'tweets_photos':{'$each':tweetids}}})


        elif opt=="nophotosurls":
            lists_nophotosurls = list(client['relevancer']['QueryList'].find({"_id":ObjectId(queryid)},{'tweets_nophotosurls':1,'userlist_nophotosurls':1}))
            for doc in lists_nophotosurls:
                userlist_nophotosurls = doc['userlist_nophotosurls']
                tweets_nophotosurls = doc['tweets_nophotosurls']

            e=client[mydb][collname].find({'$and':[{'_id':{'$in':tweets_nophotosurls}},{'source.user.name':{'$nin':userlist_nophotosurls}}]},{'_id':1, 'text':1})

            tweetids=[]
            tweets=[]
            for doc in e:
                tweets.append(doc['text'])
                tweetids.append(doc['_id'])


            f=client['relevancer']['QueryList'].update_one({"_id":ObjectId(queryid)},{'$addToSet':{'tweets_nophotosurls':{'$each':tweetids}}})

        cntr=Counter()
        for t in tweets:
            matchlist=re.findall(r'#[a-z0-9]+',t, re.I)
            for m in matchlist:
                cntr[m]+=1

        hashtaglist=[]
        for value, count in cntr.most_common()[:int(incoming["n"])]:
            exampletweets=list(client[mydb][collname].find({'text': {'$regex':re.compile(value,re.I)}},{'text':1, '_id':0}).limit(10))
            extweets=[etdict['text'] for etdict in exampletweets]

            hashtaglist.append({"hashtag": value, "numtweet":count, "exampletweets": extweets})
        return jsonify(hashtaglist=hashtaglist,numberoftweets=len(tweets))

@app.route("/exclude/<mydb>/<collname>/hashtags", methods=['POST'])
def excludehashtags(mydb, collname):
    '''
    This method excludes the tweets which has the hashtags in the hashtag list provided by training interface.

    '''
    incoming = json.loads(request.json)
    opt = incoming['opt']
    queryid = incoming['objid']

    if opt=="urls":
        lists_url = list(client['relevancer']['QueryList'].find({"_id":ObjectId(queryid)},{'tweets_urls':1,'hashtaglist_urls':1}))
        for doc in lists_url:
            hashtaglist_urls = doc['hashtaglist_urls']
            tweets_urls = doc['tweets_urls']

        extweetids = []
        for hashtag in hashtaglist_urls:
            etmp=client[mydb][collname].find({'$and':[{'_id':{'$in':tweets_urls}},{'text':{'$regex':re.compile(hashtag,re.I)}}]},{'_id':1})
            tweetidstmp=[]
            for doc in etmp:
                tweetidstmp.append(doc['_id'])

            extweetids = list(chain(tweetidstmp, extweetids))

        newtweetids = [idx for idx in tweets_urls if idx not in extweetids]

        f=client['relevancer']['QueryList'].update_one({"_id":ObjectId(queryid)},{'$addToSet':{'tweets_urls':{'$each':newtweetids}}})

    elif opt=="photos":
        lists_photos = list(client['relevancer']['QueryList'].find({"_id":ObjectId(queryid)},{'tweets_photos':1,'hashtaglist_photos':1}))
        for doc in lists_photos:
            hashtaglist_photos = doc['hashtaglist_photos']
            tweets_photos = doc['tweets_photos']

        extweetids = []
        for hashtag in hashtaglist_photos:
            etmp=client[mydb][collname].find({'$and':[{'_id':{'$in':tweets_photos}},{'text':{'$regex':re.compile(hashtag,re.I)}}]},{'_id':1})
            tweetidstmp=[]
            for doc in etmp:
                tweetidstmp.append(doc['_id'])

            extweetids = list(chain(tweetidstmp,extweetids))

        newtweetids = [idx for idx in tweets_photos if idx not in extweetids]

        f=client['relevancer']['QueryList'].update_one({"_id":ObjectId(queryid)},{'$addToSet':{'tweets_photos':{'$each':newtweetids}}})



    elif opt=="nophotosurls":
        lists_nophotosurls = list(client['relevancer']['QueryList'].find({"_id":ObjectId(queryid)},{'tweets_nophotosurls':1,'hashtaglist_nophotosurls':1}))
        for doc in lists_nophotosurls:
            hashtaglist_nophotosurls = doc['hashtaglist_nophotosurls']
            tweets_nophotosurls = doc['tweets_nophotosurls']

        extweetids = []
        for hashtag in hashtaglist_nophotosurls:
            etmp=client[mydb][collname].find({'$and':[{'_id':{'$in':tweets_nophotosurls}},{'text':{'$regex':re.compile(hashtag,re.I)}}]},{'_id':1})
            tweetidstmp=[]
            for doc in etmp:
                tweetidstmp.append(doc['_id'])

            extweetids = list(chain(tweetidstmp,extweetids))

        newtweetids = [idx for idx in tweets_nophotosurls if idx not in extweetids]

        f=client['relevancer']['QueryList'].update_one({"_id":ObjectId(queryid)},{'$addToSet':{'tweets_nophotosurls':{'$each':newtweetids}}})

    return jsonify(numberoftweets=len(newtweetids))




@app.route("/gettweetids/<mydb>/<collname>", methods=['POST'])
def getweetids(mydb, collname):
    '''
    This method gets tweets ids for urls, photos and no_photosurls.
    '''
    incoming = json.loads(request.json)
    opt = incoming["opt"]
    if opt=="urls":
            e=client[mydb][collname].find({'urls':{'$exists':True, '$not':{'$size':0}}},{'_id':1});
    elif opt=="photos":
            e=client[mydb][collname].find({'photos':{'$exists':True, '$not':{'$size':0}}},{'_id':1});
    elif opt=="nophotosurls":
            e=client[mydb][collname].find({'$and':[{'photos':{'$size':0}},{'urls':{'$size':0}}]}, {'_id':1})

    tweets=[etdict['_id'] for etdict in e]
    updated=client['relevancer']['QueryList'].update({"_id":ObjectId(incoming["objid"])}, {'$push':{'tweets_'+opt:{'$each':tweets}}})
    return jsonify(numberoftweets=len(tweets))


#Aggregations operations process data records and return computed results.

############################# Clusters

@app.route("/getdata/<mydb>/<collname>/tweetlists/<objectid>/<opt>")
def tweetlists(mydb, collname, objectid, opt):
    '''
    This method creates clusters according to tweetlist' objectID
    '''
    query=client['relevancer']['QueryList'].find({"_id":ObjectId(objectid)},{"tweets_"+opt:1})
    querylist=[]
    for doc in query:
        querylist.append(doc)
    tweetlist=list(client[mydb][collname].find({'_id':{'$in':querylist[0]["tweets_"+opt]}},{"text":1, "source":1, "date":1, "_id":0}))
    return jsonify(tweetlists=tweetlist)


@app.route("/save/<mydb>/<collname>/cluster", methods=['POST'])
def savecluster(mydb, collname):
    '''
    This method saves cluster that created previously.
    '''
    incoming = json.loads(request.json)
    clusterlist = incoming["clusterlist"]
    newcoll = client[mydb][collname].insert(clusterlist)
    return jsonify(collname=collname)


@app.route("/getcluster/<mydb>/<collname>/labeled:<label>")
def labeled(mydb, collname, label):
    '''
    This method gets labeled and unlabeled clusters with top 10 and last 10 tweets.
    '''
    collection = client[mydb][collname]
    if label=="True":
        count = collection.find({"label":{"$exists":True}}).count()
        count = count-1 if count > 0 else 1
        rnd = random.randint(0, count)
        labeled = collection.find({"label":{"$exists":True}}).limit(1).skip(rnd)
        if not labeled.count():
            return jsonify(warning="There is not any labeled cluster yet.")
        else:
            labeled = labeled[0]
            labeled["id"]=labeled.pop('_id')
            labeled["id"]=str(labeled["id"])
            tweetlist=[]
            for cl in labeled["ctweettuplelist"]:
                tweetlist.append(cl[2])
            if(len(tweetlist)>20):
                top10 = tweetlist[:10]
                last10 = tweetlist[-10:]
            else:
                top10=tweetlist
                last10=[]
            infodict={"cluster":labeled, "top10":top10, "last10":last10}
            return json.dumps(infodict, default=json_util.default)
    elif label=="False":
        count = collection.find({"label":{"$exists":False}}).count()
        count = count-1 if count > 0 else 1
        rnd = random.randint(0, count)
        unlabeled = collection.find({"label":{"$exists":False}}).limit(1).skip(rnd)

        if not unlabeled.count():
            return jsonify(warning="All clusters are labeled.")
        else:
            unlabeled = unlabeled[0]
            unlabeled["id"]=unlabeled.pop('_id')
            unlabeled["id"]=str(unlabeled["id"]) #objectid to string
            tweetlist=[]
            for cl in unlabeled["ctweettuplelist"]:
                tweetlist.append(cl[2])
            if(len(tweetlist)>20):
                top10 = tweetlist[:10]
                last10 = tweetlist[-10:]
            else:
                top10=tweetlist
                last10=[]
            infodict={"cluster":unlabeled, "top10":top10, "last10":last10}
            return json.dumps(infodict, default=json_util.default)

    else:
        return jsonify(a="Error")


@app.route('/echoParam')  #test for url query parameter.
def echoParam():

    try:
        return request.args['k']
    except KeyError:
        return "Missing parameter"


def runapi(argv):

    port = ''
    try:
        opts, args = getopt.getopt(argv,"hp:",["port="])
    except getopt.GetoptError:
        print('\nUsage: run.py -p <port_number>\n')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('Usage: run.py -p <port_number>')
            sys.exit()
        elif opt in ("-p", "--port"):
            app.run(host="127.0.0.1", port=int(arg), debug=True)
        else:
            print('\nUsage: run.py -p <port_number>\n')
            sys.exit(2)

if __name__ == '__main__':

    if(sys.version_info > (3, 0)):
        print('Python version is OK')
    else:
        print('Incompatible Python version, should be higher than version 3. Please check that you are in the correct virtual environment.')
        sys.exit()

    if(sys.argv[1:]):
        runapi(sys.argv[1:])
    else:
        print('\nError: Missing argument. Please define a port number;\nUsage: run.py -p <port_number>\n')
